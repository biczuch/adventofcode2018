#load "..\..\packages/FsLab/FsLab.fsx"

open Deedle
open FSharp.Data
open XPlot.GoogleCharts
open XPlot.GoogleCharts.Deedle
open System.IO

// Connect to the WorldBank and access indicators EU and CZ
// Try changing the code to look at stats for your country!


open System.Text.RegularExpressions
open System.Numerics

type Point = {
    position : int * int
    velocity : int * int
}

let (|Regex|_|) pattern input =
    let regexMatch = Regex.Match(input, pattern)
    printfn "%A" regexMatch
    if regexMatch.Success then
        Some (List.tail [ for g in regexMatch.Groups -> g.Value])
    else
        None

let parseLine line = 
    let pattern = @"position=<(.+?),(.+?)> velocity=<(.+?),(.+?)>"
    match line with
    | Regex pattern [px;py;vx;vy] -> {position = (int px, int py); velocity = (int vx, int vy)}
    | _ -> failwith "error parsing line"

let parseInput (input:string) = 
    input.Split([|'\n'|]) |> Array.map parseLine |> List.ofArray

let testString = "position=< 9,  1> velocity=< 0,  2>
position=< 7,  0> velocity=<-1,  0>
position=< 3, -2> velocity=<-1,  1>
position=< 6, 10> velocity=<-2, -1>
position=< 2, -4> velocity=< 2,  2>
position=<-6, 10> velocity=< 2, -2>
position=< 1,  8> velocity=< 1, -1>
position=< 1,  7> velocity=< 1,  0>
position=<-3, 11> velocity=< 1, -2>
position=< 7,  6> velocity=<-1, -1>
position=<-2,  3> velocity=< 1,  0>
position=<-4,  3> velocity=< 2,  0>
position=<10, -3> velocity=<-1,  1>
position=< 5, 11> velocity=< 1, -2>
position=< 4,  7> velocity=< 0, -1>
position=< 8, -2> velocity=< 0,  1>
position=<15,  0> velocity=<-2,  0>
position=< 1,  6> velocity=< 1,  0>
position=< 8,  9> velocity=< 0, -1>
position=< 3,  3> velocity=<-1,  1>
position=< 0,  5> velocity=< 0, -1>
position=<-2,  2> velocity=< 2,  0>
position=< 5, -2> velocity=< 1,  2>
position=< 1,  4> velocity=< 2,  1>
position=<-2,  7> velocity=< 2, -2>
position=< 3,  6> velocity=<-1, -1>
position=< 5,  0> velocity=< 1,  0>
position=<-6,  0> velocity=< 2,  0>
position=< 5,  9> velocity=< 1, -2>
position=<14,  7> velocity=<-2,  0>
position=<-3,  6> velocity=< 2, -1>"

let getPoints input = input |> parseInput 

let input = Directory.GetCurrentDirectory() + @"\Plots\Plots\input.txt"
let points = input |> File.ReadAllText |> getPoints


//10391
let seconds = [1..10391]

let rec movePoints seconds points =
    let movePoint point = 
        let {position=(x,y); velocity=(vx,vy)} = point
        { point with position = (x+vx,y+vy) }

    match seconds with
    | [] -> points
    | _::xs -> let points = points |> List.map movePoint
               movePoints xs points
  

let scatterPoints = points 
                    |> movePoints seconds 
                    |> List.map (fun { position = (x,y) } -> (x,y))
    

Chart.Scatter scatterPoints 
|> Chart.WithSize (1000, 300)
|> Chart.Show


