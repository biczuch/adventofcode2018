open System
type Direction = | Left | Right       
type ElfPosition = ElfPosition of int * Direction
 
type ElvesPositions = ElvesPositions of (ElfPosition * ElfPosition)

type Recipe = Recipe of int
with 
    static member OfChar char = char |> int |> fun (x) -> x - 48 |> Recipe
    static member GetChar (Recipe c) = c |> char
    static member GetInt (Recipe c) = c |> int

type Recipes = Recipes of Map<int,Recipe>
with
    static member AddRecipe recipe (Recipes recipes) =
        let count = recipes |> Map.count
        recipes.Add(count, recipe) |> Recipes

    static member AddRecipes recipes newRecipies = 
        newRecipies 
        |> List.fold (fun recipes recipe -> recipes |> Recipes.AddRecipe recipe ) recipes
    static member GetRecipe (Recipes recipes) (ElfPosition (i,_)) =
        recipes.TryGetValue(i)

let changeDirection (ElfPosition (pos,dir)) =
    let newDirection = match dir with
                       | Left -> Right
                       | Right -> Left
    ElfPosition(pos, newDirection)

let initRecipes() =
    [(0,Recipe 3); (1,Recipe 7)] |> Map.ofList |> Recipes

let initElvesPositions() = ((ElfPosition (0, Left)),ElfPosition(1, Right)) |> ElvesPositions

let getNextRecipes (Recipe v1, Recipe v2)= 
    (v1 + v2).ToString().ToCharArray() |> Array.map Recipe.OfChar |> List.ofArray

let getProcessedRecipes (ElvesPositions (i, j)) recipes = 
    let get = Recipes.GetRecipe recipes
    printfn "%A %A" (i, j) recipes
    let (success1, recipe1) = get(i)
    let (success2, recipe2) = get(j)

    if(success1 && success2 |> not) then
        failwith "invalid elf positions"
    let res = (recipe1, recipe2)
    printfn "%A" res
    res
    

let createNewRecipies elvesPosition recipes = 
    getProcessedRecipes elvesPosition recipes  
    |> getNextRecipes
    |> Recipes.AddRecipes recipes

let toTuple list =
    match list with
    | [] -> failwith "Could not create tuple from empty list"
    | [x1;x2] -> (x1,x2)
    | _ -> failwith "Not supported"

let rec moveElf  maxPosition steps (ElfPosition(pos, dir)) = 

    let stepForward (ElfPosition(pos, dir)) =
        match dir with
        | Right -> (ElfPosition(pos + 1, dir))
        | Left -> (ElfPosition(pos - 1, dir))
    
    
    let nextMove = moveElf maxPosition (steps - 1) 
    
    match steps with
    | 0 -> (ElfPosition(pos,dir))
    | _ -> match pos = maxPosition || pos = 0 with
           | true -> (ElfPosition(pos,dir)) |> changeDirection |> stepForward |> nextMove
           | false -> (ElfPosition(pos,dir)) |> stepForward |> nextMove
                 
    

let moveElves (ElvesPositions(pos1, pos2)) (Recipes recipes) =
    let (Recipe v1, Recipe v2) = getProcessedRecipes (ElvesPositions(pos1, pos2)) (Recipes recipes)
    let maxPos = recipes |> Map.count |> (fun x -> x - 1)
    [v1; v2] 
    |> List.zip [pos1;pos2]
    |> List.map (fun (pos, v) -> moveElf v maxPos pos )
    |> toTuple
    |> ElvesPositions

let proceed (elvesPositions:ElvesPositions) recipes =
    let newRecipes = createNewRecipies elvesPositions recipes
    let newPositions = moveElves elvesPositions newRecipes
    (newRecipes, newPositions)

let part1 input = 
    [1..input]
    |> List.fold (fun (recipes, pos) _ -> proceed pos recipes) (initRecipes(), initElvesPositions())

let printOnlyRecipes (Recipes recipes) =
    recipes |> Map.map (fun i x -> x |> Recipe.GetInt) |> Map.toList |> List.map snd

part1 2//|> fst |> printOnlyRecipes