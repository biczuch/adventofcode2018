open System;
open System.IO;

let input =
    "[1518-11-01 00:00] Guard #10 begins shift
    [1518-11-01 00:05] falls asleep
    [1518-11-01 00:25] wakes up
    [1518-11-01 00:30] falls asleep
    [1518-11-01 00:55] wakes up
    [1518-11-01 23:58] Guard #99 begins shift
    [1518-11-02 00:40] falls asleep
    [1518-11-02 00:50] wakes up
    [1518-11-03 00:05] Guard #10 begins shift
    [1518-11-03 00:24] falls asleep
    [1518-11-03 00:29] wakes up
    [1518-11-04 00:02] Guard #99 begins shift
    [1518-11-04 00:36] falls asleep
    [1518-11-04 00:46] wakes up
    [1518-11-05 00:03] Guard #99 begins shift
    [1518-11-05 00:45] falls asleep
    [1518-11-05 00:55] wakes up"

type DutyAction =
    | FallsAsleep
    | WakesUp
    | BeginsShift

type DutyLog = {
    id : int
    time: DateTime
    action: DutyAction
}

type TimeAsleep =
| Log of DutyLog
| Total of int * DutyAction

let parseId lastUsedId (lineStr:string) =
    match lineStr.Contains("#") with
    |true ->
        let idStartIndex = lineStr.IndexOf("#") + 1
        let idEndIndex = lineStr.IndexOf(" ", idStartIndex)
        lineStr.Substring(idStartIndex, idEndIndex - idStartIndex) |> Int32.Parse
    |false -> lastUsedId

let parseTime (lineStr:string) =
    let timeEndIndex = lineStr.IndexOf("]")
    let timeStartIndex = lineStr.IndexOf("[") + 1
    lineStr.Substring(timeStartIndex, timeEndIndex - timeStartIndex)
    |> DateTime.Parse

let parseInput (inputStr:string) =
    let parseLine lastUsedId (lineStr:string) =
        let id = lineStr |> parseId lastUsedId
        let time = lineStr |> parseTime

        if lineStr.Contains("falls asleep") then
            { id=id; time=time; action = FallsAsleep }
        else if lineStr.Contains("wakes up") then
            { id=id; time=time; action = WakesUp }
        else
            { id=id; time=time; action = BeginsShift }

    let folder (lastUsedId, results) (_, line) =
        let line = line |> parseLine lastUsedId
        (line.id, results @ [line])

    inputStr.Split([|'\n'|])
    |> Array.map (fun x -> (parseTime x, x))
    |> Array.sortBy (fun (t, _) -> t)
    |> Array.fold folder (0, [])
    |> snd


let toPairs list =
    let rec loop list result =
        match list with
        | [] -> result
        | [_] -> []
        | x::y::s -> loop s ((y, x)::result)
    loop (List.rev list) []

let getTimeAsleepPairs dutyLog =
    dutyLog
    |> List.groupBy (fun x -> x.id)
    |> List.map (fun (id, logs) -> let minutesAsleep = logs
                                                       |> List.filter (fun x -> x.action = FallsAsleep || x.action = WakesUp)
                                                       |> List.map (fun x -> x.time)
                                                       |> toPairs
                                   (id, minutesAsleep))

let calculateTimeAsleep dutyLog =
    dutyLog
    |> getTimeAsleepPairs
    |> List.map (fun (x, y) -> (x, y |> List.sumBy (fun (x,y) -> (y - x).Minutes)))
    |> List.sortBy (fun (x, _) -> x)

let generateAsleepTime (timeFrom:DateTime) (timeTo:DateTime) =
    let mutable tmp = timeFrom
    let mutable results = [];
    while tmp < timeTo do
        results <- tmp::results;
        tmp <- tmp.AddMinutes 1.0;
    results

let calculateMostMinuteAsleep dutyLog =
    dutyLog
    |> getTimeAsleepPairs
    |> List.map (fun (x, y) -> let sleepTimes = y |> List.map (fun (t1,t2) -> generateAsleepTime t1 t2)
                               (x, sleepTimes |> List.concat))
    |> List.map (fun (x,y) -> (x, y |> List.map (fun y -> y.Minute) |> List.countBy (fun x-> x)))
    |> List.map (fun (x, y) -> (x, y |> List.maxBy(fun (x, y)-> y) |> fst))
    |> List.sortBy (fun (x, _) -> x)

let log = input |> parseInput


let part1 input =
    let log = input |> parseInput
    let timeAsleep = log |> calculateTimeAsleep
    let mostMinutes = log |> calculateMostMinuteAsleep
    List.zip timeAsleep mostMinutes
    |> List.map ( fun ((x,y), (a,b)) -> (x,y,b, x*b))

let input = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar.ToString() + "Day3-input.txt"
