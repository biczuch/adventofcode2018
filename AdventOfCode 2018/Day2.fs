open System.IO
type Box = {
    id : string
}

type LetterCount = {
    letter: char
    count: int
}

let letterCount str letter =
    str 
    |> Array.where (fun x -> x = letter) 
    |> Array.length

let countLetters (str:string) =
    let charStr = str.ToCharArray() 
    let chars = charStr |> Array.distinct
    
    chars 
    |> Array.map (fun c -> { letter= c; count = letterCount charStr c })
    |> Array.toList

let containsNumberOfLetters number letterCounts =
    letterCounts 
        |> List.exists (fun x -> x.count = number)

let containsTwo = containsNumberOfLetters 2
let containsThree = containsNumberOfLetters 3

let containsTwoAndThree str =
    (containsTwo str, containsThree str)

let parseInput (str:string) =
    str.Split([|'\n'|]) |> Array.toList

let createBox str =
    { id = str }

let extractId box =
    box.id

let part1 strList =
    let countLettersFromBox = createBox >> extractId >> countLetters
    let twosAndThrees = strList |> List.map ( countLettersFromBox >> containsTwoAndThree)
    let twos = twosAndThrees 
               |> List.map fst 
               |> List.filter (id) 
               |> List.length
               
    let threes = twosAndThrees |> List.map snd |> List.filter (id) |> List.length
    twos * threes

let getInput path =
    path 
        |> File.ReadAllLines  
        |> List.ofArray


//Tests
let input = "abcdef\nbababc\nabbcde\nabcccd\naabcdd\nabcdee\nababab"
let checksum = input |> parseInput |> part1
let testResult = checksum = 12


//part2
let compareBox box1 box2 =
        let letters1 = box1.id.ToCharArray()
        let letters2 = box2.id.ToCharArray()
        let zippedLetters = Array.zip letters1 letters2 |> Array.mapi (fun i x -> (i, x))
        let notMatching = zippedLetters |> Array.filter (fun (i, (x,y)) -> x <> y)
        (box1, box2, notMatching)

let findNotMachingBoxes boxes = 
    let rec loop box rest result =
        match rest with
        | [] -> result
        | _  -> let comparisonResult = rest |> List.map (compareBox box)
                let newResult = result @ comparisonResult
                let x::xs = rest
                loop x xs newResult

    match boxes with
    | [] -> []
    | x::xs -> loop x xs []

let removeAtPositions (str:string) positions =
    let mutable removed = 0
    let mutable output = str
    for pos in positions do
        output <- output.Remove(pos - removed, 1)
        removed <- removed + 1
    output

let part2 input = 
            let (b1, b2, diff, count) = 
                input 
                |> List.map createBox
                |> findNotMachingBoxes
                |> List.map (fun (x,y,z) -> (x,y,z, (z |> Array.length)))
                |> List.sortBy (fun (_,_,_, count) -> count)
                |> List.head
                
            diff
            |> Array.map (fun (p, _) -> p)
            |> removeAtPositions b1.id
            
            


//Tests
let input2 = "abcde\nfghij\nklmno\npqrst\nfguij\naxcye\nwvxyz"
let x = input2
        |> parseInput
        |> part2
        
let testResult2 = x = "fgij"

// //Results
let path = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar.ToString() + "Day2-input.txt"
let result = path |> getInput |> part1

let resutl2 = path |> getInput |> part2