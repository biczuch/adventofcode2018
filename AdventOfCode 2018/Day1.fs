open System
open System.IO

type FrequencyChange = FrequencyChange of int
type FrequencyTotal = {
    Total : int
    History: int Set
}
with
    static member Create ()=
        { Total = 0; History = Set.empty }
    static member Create total =
        { Total = total; History = Set.empty }
    static member CreateFromNewTotal oldTotal newTotalVal =
        { 
            oldTotal with 
                Total = newTotalVal; 
                History = oldTotal.History |> Set.add oldTotal.Total
        }

type Frequency = 
    | Change of FrequencyChange
    | Total of FrequencyTotal
let getInput path =
    path 
        |> File.ReadAllLines  
        |> Array.map Int32.Parse
        |> List.ofArray

let freq = FrequencyChange 1

let toFrequency ints =
    ints 
    |> List.map (FrequencyChange >> Change)

let addFrequency freq1 freq2 =
    let concatTotalAndChange (change, total) =
        let (FrequencyChange x) = change
        let newTotalVal = x + total.Total
        newTotalVal 
            |> FrequencyTotal.CreateFromNewTotal total 
            |> Total
        
    match (freq1, freq2) with
    | (Change f1, Change f2) -> let (FrequencyChange x) = f1 
                                let (FrequencyChange y) = f2
                                FrequencyTotal.Create (x + y) |> Total
    | (Change f1, Total f2) -> concatTotalAndChange (f1,f2)
    | (Total f1, Change f2 ) -> concatTotalAndChange (f2,f1)
    | (Total f1, Total f2) -> (f1.Total + f2.Total) |> FrequencyTotal.CreateFromNewTotal f1 |> Total
                              

let getResultFromFrequency frequency =
    match frequency with
    | Total x -> x.Total
    | Change x -> let (FrequencyChange result) = x
                  result

let part1 frequencies = 
    frequencies 
    |> List.fold addFrequency (FrequencyTotal.Create() |> Total)

//Part II///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
let checkDuplicate total =
    let hasDuplicate = total.History |> Set.contains (total.Total)
    match hasDuplicate with
    | true -> Some total
    | false -> None


let getFirstFrequencyReachedTwice freqChanges = 
    
    let rec iterate total change freqChanges allChanges =
        let calculateNewTotal total change allChanges = 
            match allChanges with
                        | [] -> None
                        | _ ->  let newTotal = addFrequency total change
                                match newTotal with
                                | Change _ -> None
                                | Total newTotal -> Some newTotal
        
        let newTotal = calculateNewTotal total change allChanges
        match newTotal with
        | None -> None
        | Some newTotal ->  match checkDuplicate newTotal with
                            | Some duplicate -> Some duplicate
                            | None -> match freqChanges with
                                      | [] -> match allChanges with 
                                              | [] -> None 
                                              | x::xs -> iterate (newTotal |> Total) x xs allChanges
                                      | x::xs -> iterate (newTotal |> Total) x xs allChanges

    let initialTotal = FrequencyTotal.Create() |> Total
    match freqChanges with
    | [] -> None 
    | x::xs -> iterate initialTotal x xs freqChanges

let part2 = getFirstFrequencyReachedTwice

//Tests///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
let isEqual x y = x = y
let testsPart1 = 
    let act x = x |> toFrequency |> part1 |> getResultFromFrequency
    [
        [1;-2;3;1] |> act |> isEqual 3;
        [1;1;1] |> act |> isEqual 3;
        [1;1;-2] |> act |> isEqual 0;
        [-1;-2;-3] |> act |> isEqual -6;
    ]

let testsPart2 = 
    let act x = x |> toFrequency |> part2 |> fun x -> match x with | Some x -> x |> Total |> getResultFromFrequency | None -> -9999999
    [
        [1;-1] |> act |> isEqual 0;
        [3;3;4;-2;-4] |> act |> isEqual 10;
        [-6;3;8;5;-6] |> act |> isEqual 5;
        [7;7;-2;-7;-4] |> act |> isEqual 14; 
    ]

//Results///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
let path = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar.ToString() + "Day1-input.txt"
let inputFrequencies = path |> getInput |> toFrequency
let result1 = inputFrequencies |> part1 |> getResultFromFrequency
let firstFreqReachedTwice = inputFrequencies |> getFirstFrequencyReachedTwice
let result2 = match firstFreqReachedTwice with
              | None -> None
              | Some freq -> Some freq.Total


printfn "Part I - Frequency : %d" result1
printfn "Part II - First frequency reached for the second time: %A" result2