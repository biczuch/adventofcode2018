open System.Text.RegularExpressions
open System.IO
open System.Text
open System

let testInput = "Step C must be finished before step A can begin.
Step C must be finished before step F can begin.
Step A must be finished before step B can begin.
Step A must be finished before step D can begin.
Step B must be finished before step E can begin.
Step D must be finished before step E can begin.
Step F must be finished before step E can begin."

let regex = System.Text.RegularExpressions.Regex

let (|Regex|_|) pattern input =
    let m = Regex.Match(input, pattern)
    if m.Success then Some(List.tail [ for g in m.Groups -> g.Value ])
    else None
    
let parseLine line =
    match line with
    | Regex @"\Step (.) must be finished before step (.) can begin" [prev;next] -> (prev |> char, next |> char)
    | _ -> failwith "Invalid input"

let parseInput (input:string) =
    input.Split([|'\n'|])
    |> Array.map parseLine
    |> List.ofArray

testInput |> parseInput

let getStartPoint instructions =
    let steps = instructions |> List.map fst
    let previousSteps = instructions |> List.map snd
    steps |> List.except previousSteps

let rec findNextStep instructions previousStep =
    instructions 
    |> List.filter (fun (step, _) -> step = previousStep)

let groupSteps steps =
    steps 
    |> List.collect (fun (x,y) -> [x;y]) 
    |> List.distinct
    |> List.map (fun x -> (x, steps |> List.filter (fun (prev,next) -> next = x) |> List.map fst))

let takeMaxOf count list =
    let rec loop count list result =
        match list with
        | [] -> result
        | x::xs -> match count with
                   | 0 -> result
                   | _ -> loop (count-1) xs (x::result)
    loop count list [] //|> List.rev

let findEmptyGroupedStepsForWorkers numberofWorkers groupedSteps = 
    groupedSteps 
    |> List.filter (fun (_, prev) -> (List.isEmpty prev))
    |> List.map fst
    |> List.sort
    |> takeMaxOf (numberofWorkers)

let filterGroupedStepsBasedOnProcessed processed groupedSteps = 
        groupedSteps
        |> List.map (fun (x, list) -> (x, list |> List.filter (fun y -> processed |> List.contains y |> not)))
        |> List.filter (fun (x, _) -> processed |> List.contains x |> not)

let findCorrectOrder groupedSteps =
    let rec loop groupedSteps result =
        match groupedSteps with
        | [] -> result
        | _ ->  let toProcess = groupedSteps |> findEmptyGroupedStepsForWorkers 1
                let rest = groupedSteps |> filterGroupedStepsBasedOnProcessed toProcess
                let newResult = result @ [toProcess |> List.head]
                loop rest newResult
    loop groupedSteps []

let part1 input =
    input 
    |> parseInput
    |> groupSteps
    |> findCorrectOrder
    
let inputPath = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar.ToString() + "Day7-input.txt"
let input = inputPath |> File.ReadAllText

input |> part1 |> List.fold (fun s i -> s + i.ToString()) ""

type Worker = {
    id : int
    workingOn: char option
    timeToComplete: int
}

let createWorkers number =
    List.init number (fun i -> { id = i; workingOn = None; timeToComplete = 0})

let doWork workers =
    let decreaseTime worker =
        match worker.workingOn with
        | Some _ -> { worker with timeToComplete = worker.timeToComplete - 1 }
        | None -> worker
    
    let getCompletedTasks workers =
        workers 
        |> List.filter (fun x -> x.timeToComplete = 0) 
        |> List.map (fun x -> x.workingOn)
        |> List.map (fun x -> x.Value)
    
    let finishJob worker =
        match worker.timeToComplete with
        | 0 -> { worker with workingOn = None }
        | _ -> worker

    let workers = workers |> List.map decreaseTime
    let completedTasks = workers |> getCompletedTasks
    let workers = workers |> List.map finishJob
    (workers, completedTasks)


let oneTick workers groupSteps totalTime =
    let workers = workers |> doWork

let workersStillWork workers = 
     workers |> List.forall (fun x -> x.workingOn = None)

let getTimeForCompletion baseTimeForCompletion workers groupSteps =
     let rec loop workers groupSteps totalTime =
         match groupSteps with
         | [] -> match workers |> workersStillWork with
                 | false -> totalTime
                 | true -> let (workers, groupSteps, totalTime) = oneTick workers groupSteps totalTime
                           loop workers groupSteps totalTime
         | _ -> 0
     loop workers groupSteps 0

let part2 workersNumber baseTimeToMake input =
    let workers = createWorkers workersNumber
    input 
    |> parseInput 
    |> groupSteps
    |> getTimeForCompletion workers baseTimeToMake

let xxx = testInput |> part2 2 0