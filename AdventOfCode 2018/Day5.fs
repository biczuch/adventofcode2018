open System.IO
open System;

let rec triggerReaction polymer =

    let isReacting (unitA:char,unitB:char) =
        match (unitA, unitB) with
        | (x, y) when Char.ToLower x = Char.ToLower y -> x <> y
        | _ -> false  

    let reaction unit polymer =
        match polymer with
        | [] -> [unit]
        | x::xs -> match isReacting (x, unit) with
                   | true -> xs
                   | false -> unit::polymer

    let afterReaction = List.foldBack reaction polymer []

    if polymer = afterReaction then
        afterReaction
    else 
        triggerReaction afterReaction
        
let solvePart1 (polymerString:string) =
    polymerString.ToCharArray() 
    |> List.ofArray 
    |> triggerReaction
    |> Seq.length

let removeUnitFromPolymer polymer unit =
    polymer 
    |> List.filter (fun x -> x <> Char.ToLower unit && x <> Char.ToUpper unit) 

let solvePart2 (polymerString:string) =
    let polymer = polymerString.ToCharArray() |> List.ofArray
    let letters = polymer
                  |> List.map Char.ToLower
                  |> List.distinct
    
    letters 
    |> List.map (fun x -> (x, x |> removeUnitFromPolymer polymer |> triggerReaction |> List.length))
    |> List.minBy (fun (x, length) -> length)
    |> snd   
    
let inputPath = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar.ToString() + "Day5-input.txt"
let input = inputPath |> File.ReadAllText
let solution = (solvePart1 input, solvePart2 input)

