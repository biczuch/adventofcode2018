open System;
open System.IO

let testInput = "1, 1
1, 6
8, 3
3, 4
5, 5
8, 9
"

let toTuple array =
    match array with
    | [] -> failwith "Array is empty"
    | x::y::xs -> (x,y)

let parseInput (input:string) =
    input.Split('\n') 
    |> Array.filter (fun x -> x <> String.Empty)
    |> Array.map (fun x -> x.Split(',') |> Array.map int |> List.ofArray |> toTuple )
    |> List.ofArray

let generateBorderPoints boundaries =
    let [(x,y); (a,b)] = boundaries

    let upper = [y..b] |> List.map (fun p -> (x,p))
    let left = [x..a] |> List.map (fun p -> (p,y))
    let right = [x..a] |> List.map (fun p -> (p,b))
    let down = [y..b] |> List.map (fun p -> (a,p))
    
    [upper;left;right;down] |> List.concat

let calculateManhattanDistance (p1:int,p2:int) (q1:int,q2:int) =
    Math.Abs(p1-q1) + Math.Abs(p2-q2)

let findInfiniteCoordinates boundaries points =

    let findMinDistanceFromBorder points borderPoint = 
        points 
        |> List.map (fun p -> (p, calculateManhattanDistance p borderPoint))  
        |> List.minBy (fun (_, dist) -> dist)

    boundaries 
    |> generateBorderPoints
    |> List.map (findMinDistanceFromBorder points >> fst)
    |> List.distinct

let findBoundaryCoordinates points =
    let minX = points |> List.minBy (fun (x,y) -> x) |> fst
    let maxX = points |> List.maxBy (fun (x,y) -> x) |> fst
    let minY = points |> List.minBy (fun (x,y) -> y) |> snd
    let maxY = points |> List.maxBy (fun (x,y) -> y) |> snd
    [(minX-1, minY-1);(maxX+1, maxY+1)]

let generateMatrix boundaries =
    let [(x,y);(a,b)] = boundaries
    let mutable matrix = []

    for i in [x..a] do
        for j in [y..b] do
            matrix <- (i,j)::matrix 
    
    matrix |> List.rev

type MatrixPoint =
    | Single of (int * int) * int
    | Multiple of (int*int) list * int

let pointDistanceFolder state item =
    match state with
    | [] -> [item]
    | x::xs ->  let distItem = snd x
                let distState = snd item
                if distItem > distState then
                    [item]
                else if distItem < distState then
                    state
                else
                    item::state

let getMaxDistances (x, dists) = 
    let maxDists = dists 
                  |> List.sortByDescending (fun (p, dist) -> dist) 
                  |> List.fold pointDistanceFolder []
    let point = match maxDists |> List.length with
                | x when x > 1 -> Multiple (maxDists |> List.map fst, maxDists |> List.head |> snd )
                | x when x = 1 -> Single (maxDists |> List.exactlyOne)
                | _ -> failwith "should find at lest one"
    (x, point)

let calculatemanhattanDistancesForMatrixPoint coordinates matrixPoint  =
    let manhattanDistances = coordinates 
                            |> List.map (fun p -> (p, calculateManhattanDistance matrixPoint p))
    (matrixPoint, manhattanDistances)

let onlySingle matrixPoint =
    let (matrixPoint, point) = matrixPoint
    match point with
    | Single (coordinates, distance)-> Some ((matrixPoint, coordinates, distance))  
    | Multiple _ -> None

let isNotInfinite infiniteCoords coords =
    infiniteCoords |> List.contains coords |> not

let part1 input =
    let coordinates = input |> parseInput 
    let boundaries = findBoundaryCoordinates coordinates
    let infiniteCoordinates = coordinates |> findInfiniteCoordinates boundaries 
    
    boundaries 
    |> generateMatrix
    |> List.map (calculatemanhattanDistancesForMatrixPoint coordinates >> getMaxDistances) 
    |> List.choose onlySingle
    |> List.filter (fun (_, coords, _) -> isNotInfinite infiniteCoordinates coords) 
    |> List.countBy (fun (_, coords, _) -> coords)
    |> List.maxBy snd


let inputPath = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar.ToString() + "Day6-input.txt"
let input = inputPath |> File.ReadAllText |> part1

testInput |> part1


let part2 input =
    let coordinates = input |> parseInput 
    let boundaries = findBoundaryCoordinates coordinates
        
    boundaries
    |> generateMatrix
    |> List.map (calculatemanhattanDistancesForMatrixPoint coordinates)
    |> List.map (fun (x, distList) -> (x, distList |> List.map snd))
    |> List.map (fun (x, distList) -> (x, distList |> List.sum))
    |> List.filter (fun (x, totalDist) -> totalDist < 10000)
    |> List.length
    

inputPath |> File.ReadAllText |> part2

    