open System.Text.RegularExpressions

type Pot =
    | Empty
    | Flower
with 
    static member ofChar = function
        | '.' -> Empty
        | '#' -> Flower
        | _ -> failwith "Error creating Pot from char"
    

let (|Regex|_|) pattern input =
        let m = Regex.Match(input, pattern)
        if m.Success then Some(List.tail [ for g in m.Groups -> g.Value ])
        else None

let toCharList (str:string) =
    str.ToCharArray() |> List.ofArray

let parseInitialState state =
    match state with
    | Regex "initial state: (.+)" [initialState] -> initialState 
                                                    |> toCharList 
                                                    |> List.mapi (fun i x -> (i, Pot.ofChar x)) 
                                                    |> Map.ofList
    | _ -> failwith "Error parsing initial state"

let parseRules rules =
    let parseRule rule =
        match rule with
        | Regex "(.+) => (.)" [input; output] -> let input = input |> toCharList |> List.map Pot.ofChar
                                                 let output = output.[0] |> Pot.ofChar
                                                 (input, output)
        | _ -> failwith "Error parsing rule:" rule

    rules |> List.map parseRule

let parseInput (input:string) =
    let (initialState::_::rules) = input.Split([|'\n'|]) |> List.ofArray

    let initialState = initialState |> parseInitialState 
    let rules = rules |> parseRules
    (initialState, rules)

let get pots i =
    pots |> Map.tryFind i |> Option.defaultValue Empty

let getPotWindows pots =
    let keys = pots |> Map.toList |> List.map (fun (k, _) -> k)
    let min = keys |> List.min
    let max = keys |> List.max

    let getPot = get pots
    
    [min..max]
    |> List.map (fun i -> (i, [getPot(i-2); getPot(i-1); getPot(i); getPot(i+1); getPot(i+2)]))

let getMiddlePot potWindow = 
    match potWindow with
    | [_;_;pot;_;_] -> pot
    | _ -> failwith "Invalid pot window"

let matchRule rules potWindow =
   let rule = rules |> List.tryFind (fun (rule, _) -> rule = potWindow)
   match rule with
   | Some (_, output) -> output
   | None -> Empty

let addAdditionalPots pots =
    let keys = pots |> Map.toList |> List.map (fun (k, _) -> k)
    let min = keys |> List.min
    let max = keys |> List.max

    [(min-2, Empty);(min-1, Empty);(max+1, Empty);(max+2, Empty)]
    |> List.fold (fun (s:Map<int,Pot>) p -> s.Add(p)) pots

let processGeneration rules pots = 
    pots 
    |> addAdditionalPots
    |> getPotWindows
    |> List.map (fun (i, window) -> (i, matchRule rules window))
    |> Map.ofList

let sumIndexes pots =
    pots 
    |> Map.toList 
    |> List.filter (fun (_,pot) -> pot = Flower)
    |> List.map fst |> List.map int |> List.sum

let printPots pots =
    let toChar pot =
        match pot with
        | Empty -> '.'
        | Flower -> '#'
    
    let rec skipFirstDots chars =
        match chars with
        | [] -> []
        | x::xs -> if x = '.' then skipFirstDots xs else chars

    pots 
    |> Map.toList 
    |> List.map (fun (i, x) -> x |> toChar)
    |> skipFirstDots
    |> List.fold (fun s p -> s + p.ToString()) ""

let part1 input =
    let (initialState, rules) = input |> parseInput
    [1..20]
    |> List.fold (fun generation _ -> processGeneration rules generation) initialState
    |> sumIndexes

let removeEmptyPotsFromEnds (generation:Map<int, Pot>) =
    let rec removeEmpty (generationList: (int * Pot) list) =
        match generationList with
        | [] -> []
        | (i,p)::xs when p = Empty -> xs  
        | (i, p)::_ when p = Flower -> generationList
        | _ -> failwith "remove empty"
    generation |> Map.toList |> removeEmpty |> List.rev |> removeEmpty |> List.rev |> Map.ofList

let part2 input =
    let (initialState, rules) = input |> parseInput
    [1..1000]
    |> List.fold (fun generation i -> let x = processGeneration rules generation |> removeEmptyPotsFromEnds
                                      x |> printPots |> printfn "%d: %s" i
                                      x) initialState
    |> Map.toList 
    |> List.filter (fun (_, pot) -> pot = Flower)
    |> List.map (fst >> bigint)
    |> List.map ((+) (50000000000I - 1000I))
    |> List.sum

    

let input = "initial state: #..######..#....#####..###.##..#######.####...####.##..#....#.##.....########.#...#.####........#.#.

#...# => .
##.## => .
###.. => .
.#### => .
#.#.# => .
##..# => #
..#.# => #
.##.. => #
##... => #
#..## => #
#..#. => .
.###. => #
#.##. => .
..### => .
.##.# => #
....# => .
##### => .
#.### => .
.#..# => .
#.... => .
...## => .
.#.## => .
##.#. => #
#.#.. => #
..... => .
.#... => #
...#. => #
..#.. => .
..##. => .
###.# => .
####. => .
.#.#. => ." 

(input |> part1, input |> part2)