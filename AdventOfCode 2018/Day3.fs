open System;
open System.IO

type Id = Id of int

type Tile = 
| Empty
| Claimed of Id
| ClaimedMultipleTimes of int * Id list

type Claim = {
    id : Id
    position : int * int
    size : int * int
}

let parseClaimString (str:string) =
    let removeChars chars (str:string) =
        let mutable output = str
        for char in chars do
            output <- output.Replace(char.ToString(), "")
        output

    let cleanedStr = str |> removeChars [|'#';'@';':'|]
    let parts = cleanedStr.Split([|' '|]) |> Array.filter (fun x -> x <> "")
    
    let id = parts.[0] |> Int32.Parse
    let position = parts.[1].Split([|','|]) |> Array.map Int32.Parse
    let size = parts.[2].Split([|'x'|]) |> Array.map Int32.Parse

    {
        id = Id id; 
        position = (position.[0], position.[1]); 
        size = (size.[0], size.[1])
    }

let addTiles t1 t2 =

    let addClaimedMultipleTimesAndClaimed 
        (ClaimedMultipleTimes(x,xs)) 
        (Claimed(id)) =
        ClaimedMultipleTimes (x + 1, id::xs)

    match (t1, t2) with
    | (Empty, Empty) -> Empty
    | (Empty, Claimed _) -> t2
    | (Claimed _, Empty) -> t1
    | (Empty, ClaimedMultipleTimes _) -> t2
    | (ClaimedMultipleTimes _, Empty) -> t1
    | (ClaimedMultipleTimes _, Claimed _) -> addClaimedMultipleTimesAndClaimed t1 t2
    | (Claimed _, ClaimedMultipleTimes _) -> addClaimedMultipleTimesAndClaimed t2 t1
    | (ClaimedMultipleTimes (x,xs), ClaimedMultipleTimes (y,ys)) -> ClaimedMultipleTimes (x + y, xs @ ys)
    | (Claimed x, Claimed y) -> ClaimedMultipleTimes (2, [x;y])

let getPositionsToUpdate claim =
    let xPos = [fst claim.position..fst claim.position + fst claim.size - 1]
    let yPos = [snd claim.position..snd claim.position + snd claim.size - 1]
    let positionsToUpdate = xPos |> List.map (fun x -> yPos |> List.map (fun y -> (y,x)))
    positionsToUpdate |> List.concat

let claimFabric (fabric:Tile array array) claim =
    let positionsToUpdate = getPositionsToUpdate claim
    let iterFn (x,y) =
        let tile = fabric.[x].[y]
        let newTile = Claimed claim.id 
        fabric.[x].[y] <- addTiles newTile tile

    positionsToUpdate |> List.iter iterFn

let createFabric size =
    Array.init size (fun _ -> Array.init size (fun _ -> Empty))

let countClaimedMultipleTimes fabric =
    let folder state obj =
        match obj with
        | ClaimedMultipleTimes _ -> state + 1
        | _ -> state

    fabric |> Array.concat |> Array.fold folder 0

let getInput path =
    path 
        |> File.ReadAllLines  
        |> List.ofArray

let printTile tile =
    match tile with
    | ClaimedMultipleTimes _ -> "X"
    | Empty -> "."
    | Claimed (Id x)-> x.ToString()

let printFabric (fabric:Tile array array) =
    fabric |> Array.map (fun x -> x |> Array.map printTile)


let input = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar.ToString() + "Day3-input.txt"

let fabric = createFabric 1000

let claims = input |> getInput |> List.map parseClaimString

claims |> List.iter (fun c -> claimFabric fabric c)


fabric |> printFabric 
fabric |> countClaimedMultipleTimes

//part2
let checkFullCoverage (fabric:Tile array array) claim =
    let isClaimedType x =
        match x with
        | Claimed _-> true
        | _ -> false
    
    let hasClaimId (Claimed x) = 
        x = claim.id

    let fabricCoverage = fabric 
                            |> Array.concat 
                            |> Array.filter isClaimedType
                            |> Array.filter hasClaimId
                            |> Array.length
    let (sizeX, sizeY) = claim.size
    (sizeX * sizeY) = fabricCoverage

let fullyCoveredClaim = claims |> List.filter (checkFullCoverage fabric)